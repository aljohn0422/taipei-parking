interface SiteRoot {
    data: ParkingLotData
}

export interface ParkingLotData {
    UPDATETIME: string
    park: Park[]
}

export interface Park {
    id: string
    area: string
    name: string
    summary: string
    address: string
    tel: string
    payex: string
    serviceTime: string
    tw97x: string
    tw97y: string
    totalcar: number
    totalmotor: number
    Pregnancy_First?: string
    Handicap_First?: string
    FareInfo: FareInfo
    EntranceCoord: EntranceCoord
    totallargemotor?: string
    ChargingStation?: string
}

interface FareInfo {
    WorkingDay?: PeriodFare[]
    Holiday?: PeriodFare[]
}

interface PeriodFare {
    Period: string
    Fare: string
}

interface EntranceCoord {
    EntrancecoordInfo?: EntrancecoordInfo[]
}

interface EntrancecoordInfo {
    Xcod: string
    Ycod: string
    Address: string
}


export async function parseSiteInfo() {
    const res = await fetch('https://tcgbusfs.blob.core.windows.net/blobtcmsv/TCMSV_alldesc.json')
    let data = await res.json() as SiteRoot;

    return data.data;
}

