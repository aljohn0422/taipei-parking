interface LiveDataRoot {
    data: LiveData
}

export interface LiveData {
    UPDATETIME: string
    park: Park[]
}

export interface Park {
    id: string
    availablecar: number
    availablemotor?: number
    availablebus?: number
    ChargeStation?: ChargeStation
}

interface ChargeStation {
    scoketStatusList: ScoketStatusList[]
}

interface ScoketStatusList {
    spot_abrv: string
    spot_status: string
}

export async function parseLiveInfo() {
    const url = 'https://tcgbusfs.blob.core.windows.net/blobtcmsv/TCMSV_allavailable.json';
    const res = await fetch(url);
    const data = await res.json() as LiveDataRoot;

    let mem: { [key: string]: Park } = {}
    data.data.park.forEach(park => {
        mem[park.id] = park
    })
    return {
        UPDATETIME: data.data.UPDATETIME,
        data: mem,
    };
}
