import { json } from "@sveltejs/kit";
import { parseSiteInfo, type ParkingLotData } from "$lib/siteinfo";

let data: ParkingLotData | undefined = undefined

async function updateSiteInfo() {
    try {
        console.log("updating site info");
        const _data = await parseSiteInfo()
        console.log(`updated: ${_data.UPDATETIME}`);
        return _data
    } catch (error) {
        console.error(error);
        console.error("failed to update live info. skipped.")
        return
    }
}

(async () => {
    setInterval(async () => {
        const _data = await updateSiteInfo();
        data = _data ? _data : data
    }, 60 * 1000)
})()

export const GET = (async () => {
    if (!data) data = await updateSiteInfo();
    return json(data);
}) 