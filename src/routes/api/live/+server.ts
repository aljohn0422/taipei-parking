import { json } from "@sveltejs/kit";
import { parseLiveInfo, type LiveData } from "$lib/liveinfo";

let data: LiveData | undefined = undefined

async function updateLiveInfo() {
    try {
        console.log("updating live info");
        const _data = await parseLiveInfo()
        console.log(`updated: ${_data.UPDATETIME}`);
        return _data
    } catch (error) {
        console.error(error);
        console.error("failed to update live info. skipped.")
        return
    }
}

(async () => {
    setInterval(async () => {
        const _data = await updateLiveInfo();
        data = _data ? _data : data
    }, 60 * 1000)
})()

export const GET = (async () => {
    if (!data) data = await updateLiveInfo();
    return json(data);
}) 