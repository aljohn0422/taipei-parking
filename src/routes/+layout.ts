import type { ParkingLotData } from '$lib/siteinfo';
import type { Park } from '$lib/liveinfo';

interface LiveData {
    UPDATETIME: string,
    data: { [key: string]: Park }
}

export const load = (async ({ fetch }) => {
    const reslive = await fetch('/api/live');
    const livedata = await reslive.json();
    const reslots = await fetch('/api/lots');
    const sitedata = await reslots.json();

    return {
        livedata: livedata as LiveData,
        sitedata: sitedata as ParkingLotData
    };
});